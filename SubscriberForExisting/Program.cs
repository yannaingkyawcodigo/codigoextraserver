﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;

namespace SubscriberForExisting
{
    class Program
    {
        static void Main(string[] args)
        {
            var count = 0;
            var id = 0;
            var deviceType = 0;
            var token = string.Empty;
            var connectionString = "Data Source=10.97.128.196;Initial Catalog=Donki_Production;User ID=sa;Password=P@ssw0rd;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            Console.WriteLine("Enter Offset : ");
            var offset =  int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Length : ");
            var length = int.Parse(Console.ReadLine());

            for (int i = offset; i < (offset+length); i++)
            {
                Console.WriteLine("Getting for index : " + i);
                DataTable dt = new DataTable();
                using (SqlConnection sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();
                    new SqlDataAdapter(new SqlCommand("select * from tbl_Member_Push_Device where is_valid_token is null order by id offset " + i+" ROW fetch next 1 ROWS ONLY", sqlConnection)).Fill(dt);
                }
                token = (string) dt.Rows[0]["device_id"];
                id = (int)dt.Rows[0]["id"];
                deviceType = (int)dt.Rows[0]["device_type"];
                var isValid = CheckToken(token);
                if (isValid)
                {
                    var device = string.Empty;
                    var channel = "all-hk-en-";
                    if (deviceType == 1)
                    {
                        device = "ios";
                    }
                    else {
                        device = "android";
                    }

                    channel += device;

                    var result = SubScribe(device,token);
                    if (result) {
                        Console.WriteLine("Subscribe Token : " + token + " with id [" + id + "]");
                        using (SqlConnection sqlConnection = new SqlConnection(connectionString))
                        {
                            sqlConnection.Open();
                            new SqlCommand("update tbl_Member_Push_Device set is_valid_token = 1, last_check_time = GETDATE(),subscribe_channel='"+channel+"' where id = " + id, sqlConnection).ExecuteNonQuery();
                        }
                    }
                    
                }
                else {
                    Console.WriteLine("Deleting Token : " + token + " with id [" + id + "]");
                    using (SqlConnection sqlConnection = new SqlConnection(connectionString))
                    {
                        sqlConnection.Open();
                        new SqlCommand("update tbl_Member_Push_Device set is_valid_token = 0, last_check_time = GETDATE() where id = "+id, sqlConnection).ExecuteNonQuery();
                    }
                }
            }
            
        }

        

        static bool CheckToken(string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "https://localhost:6001/api/PushNotification/CheckToken/" + token);
            try
            {
                using (var client = new HttpClient())
                {
                    var result = client.SendAsync(request).Result;
                    if (result.StatusCode == HttpStatusCode.OK)
                    {   
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            catch (Exception err)
            {
                Console.WriteLine(err);
            }
            return false;
        }

        static bool SubScribe(string device,string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "https://localhost:6001/api/PushNotification/SubscribeTopic/HongKong/"+device+"/en/"+ token);
            try
            {
                using (var client = new HttpClient())
                {
                    var result = client.SendAsync(request).Result;
                    if (result.StatusCode == HttpStatusCode.OK)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            catch (Exception err)
            {
                Console.WriteLine(err);
            }
            return false;
        }
    }
}

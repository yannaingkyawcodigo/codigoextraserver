﻿using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PushNotificationServer.DatabaseUtility;
using PushNotificationServer.FirebaseUtility;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.IO;

namespace PushNotificationServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            //Hangfire
            var HangFireConnectionString = Configuration["ConnectionString:HangfireServer"];
            services.AddHangfire(x => x.UseSqlServerStorage(HangFireConnectionString));
            services.AddHangfireServer();

            //Swagger
            services.AddSwaggerGen(c =>
            {

                c.SwaggerDoc("v1", new Info
                {
                    Title = "Codigo PushNotification Server API Documentation",
                    Version = "v1",

                });
                // Configure Swagger to use the xml documentation file
                var xmlFile = Path.ChangeExtension(typeof(Startup).Assembly.Location, ".xml");
                c.IncludeXmlComments(xmlFile);

                
            });

            services.AddTransient<IFirebaseUtility, FirebaseUtility.FirebaseUtility>();
            services.AddTransient<IDatabaseUtility, DatabaseUtility.DatabaseUtility>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Codigo PushNotification Server");

            });
            app.UseHangfireServer();
            app.UseHangfireDashboard("/HangFireDashboard");
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}

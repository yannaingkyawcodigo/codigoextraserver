﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PushNotificationServer.FirebaseUtility;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PushNotificationServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PushNotificationController : Controller
    {
        static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(PushNotificationController));

        private readonly IFirebaseUtility fireBaseUtility;

        public PushNotificationController(IFirebaseUtility fireBaseUtility)
        {
            this.fireBaseUtility = fireBaseUtility;
        }      

        [HttpPost("CheckToken/{token}")]
        public IActionResult CheckToken(string token)
        {
            var isValid = fireBaseUtility.CheckToken(token);
            if (isValid)
                return Ok();
            else
                return NotFound();
        }

        [HttpPost("SubscribeTopic/{country}/{device}/{language}/{token}")]
        public IActionResult SubscribeTopic(string country,string device,string language,string token)
        {
            fireBaseUtility.SubscribeTopic(country,device,language,token);
            return Ok();
        }


        [HttpPost("UnsubscribeTopic/{country}/{device}/{language}/{token}")]
        public IActionResult UnsubscribeTopic(string country, string device, string language, string token)
        {
            fireBaseUtility.UnsubscribeTopic(country, device, language, token);
            return Ok();
        }

        [HttpPost("SendTopic/{country}/{device}/{language}/{pushId}")]
        public IActionResult SendTopicSingapore(string country, string device, string language, string pushId)
        {
            this.fireBaseUtility.SendMessageToTopic(country, device, language, Guid.Parse(pushId));
            return Ok();
        }

    }
}

﻿using Hangfire;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PushNotificationServer.DatabaseUtility;
using PushNotificationServer.Enum;
using PushNotificationServer.GeneralUtility;
using PushNotificationServer.HangFireUtility;
using PushNotificationServer.Model;
using System;
using System.Net;
using System.Net.Http;
using System.Text;

namespace PushNotificationServer.FirebaseUtility
{
    public interface IFirebaseUtility 
    {
        void SubscribeTopic(string country, string device, string language, string token);
        void UnsubscribeTopic(string country, string device, string language, string token);
        void SendMessageToTopic(string country, string device, string language, Guid PushId);
        bool CheckToken(string token);
    }

    public class FirebaseUtility : IFirebaseUtility
    {
        private readonly string Authorization;
        private readonly string SendNotificationURL;
        private readonly string SubscribeURL;
        private readonly string UnSubscribeURL;
        private readonly string AllHKUserTopic = "all-hk";
        private readonly string AllSGUserTopic = "all-sg";
        private readonly string ApplicationConnectionString;
        private readonly string HKDatabase;
        private readonly string SGDatabase;
        private readonly string CheckTokenURL;

        private readonly IDatabaseUtility databaseUtility;

        static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FirebaseUtility));
        public FirebaseUtility(IConfiguration config,IDatabaseUtility databaseUtility)
        {
            Authorization = config["Firebase:Authorization"];
            SendNotificationURL = config["Firebase:SendULR"];
            SubscribeURL = config["Firebase:SubscribeTopic"];
            CheckTokenURL = config["Firebase:CheckTokenURL"];
            UnSubscribeURL = config["Firebase:UnSubscribeTopic"];            
            ApplicationConnectionString = config["ConnectionString:ApplicationServer"];
            HKDatabase = config["ApplicationDatabase:HongKong"];
            SGDatabase = config["ApplicationDatabase:Singapore"];

            this.databaseUtility = databaseUtility;
        }

        public bool CheckToken(string token)
        {
            
            var request = new HttpRequestMessage(HttpMethod.Get, CheckTokenURL+token);
            request.Headers.TryAddWithoutValidation("Authorization", Authorization);
            try
            {
                using (var client = new HttpClient())
                {
                    var result = client.SendAsync(request).Result;
                    if (result.StatusCode == HttpStatusCode.OK)
                    {
                        log.Info("Valid Token " +token);
                        log.Info(result.Content.ReadAsStringAsync().Result);
                        return true;
                    }
                    else
                    {

                        log.Error("Invalid token :" + token);
                        log.Error(result.Content.ReadAsStringAsync().Result);
                        return false;
                    }
                }

            }
            catch (Exception err)
            {
                log.Error("Invalid token :" + token);
                log.Error(err);
            }
            return false;
        }

        public void SendMessageToTopic(string country,string device,string language,Guid PushId )
        {
            var DatabaseName = string.Empty;
            var topicName = string.Empty;
            var timeZone = 0;
            MobileDeviceType deviceType = MobileDeviceType.iOS;

            if (device == "ios")
            {
                deviceType = MobileDeviceType.iOS;
            }
            else {
                deviceType = MobileDeviceType.Android;
            }
            if (country == "HongKong")
            {
                DatabaseName = HKDatabase;                
                timeZone = 1;
                topicName = AllHKUserTopic + language + device;
            }
            else {
                DatabaseName = SGDatabase;
                timeZone = 2;
                topicName = AllSGUserTopic + language + device;
            }
            var push = databaseUtility.GetPushInfo(PushId.ToString(), DatabaseName);
            if (push == null) {
                log.Error("Can't Read PushNotification!");
                return;
            }
            var pushTopic = new PushTopicModel {
                Authorization = this.Authorization,
                Body = push.Push_Notification_Content,
                Title = push.Push_Notification_Title,
                PushId = PushId,
                ImageUrl = push.Push_Image,
                MediaType = 1,
                NotificationType = 6,
                Song = "NotiDMilesBellSound.mp3",
                PushURL = SendNotificationURL,
                TopicName = topicName,
                DeviceType = deviceType
            };
            
            if (push.Push_Schedule.HasValue)
            {
                log.Info("Push is schedule Push! >> "+PushId);
                var differTime = TimeZoneDifferences.GetLocalTimeWithCountry(timeZone) - push.Push_Schedule.Value;
                //SchedulePush
                var job = BackgroundJob.Schedule(() => new HangfirePusher().ScheduleSendToTopic(pushTopic),differTime);
                log.Info("Push is schedule with  >> " + job+" Run in Next "+ differTime.TotalSeconds + "sec");
            }
            else {
                //Just Push
                new HangfirePusher().ScheduleSendToTopic(pushTopic);
            }
        }

        public void SubscribeTopic(string country, string device, string language, string token)
        {

            
            var topicName = string.Empty;
            
            if (country == "HongKong")
            {
                topicName = AllHKUserTopic + language + device;
            }
            else
            {
                topicName = AllSGUserTopic + language + device;
            }
            var body = new
            {
                to = "/topics/" + topicName,
                registration_tokens = new string[] { token }
            };
            var JsonBody = JsonConvert.SerializeObject(body);

            var request = new HttpRequestMessage(HttpMethod.Post, SubscribeURL);
            request.Headers.TryAddWithoutValidation("Authorization", Authorization);
            request.Content = new StringContent(JsonBody, Encoding.UTF8, "application/json");
            try
            {
                using (var client = new HttpClient())
                {
                    var result = client.SendAsync(request).Result;
                    if (result.StatusCode == HttpStatusCode.OK)
                    {
                        log.Info("Subscribe  to " + topicName + " Sucess!");
                        log.Info(token);
                    }
                    else
                    {
                        
                        log.Error("Subscribe to " + topicName + " Failed!");
                        log.Error(token);
                    }
                }

            }
            catch (Exception err)
            {
                log.Error("Subscribe to " + topicName + " Failed!");
                log.Error(token);
                log.Error(err);
            }
        }

        public void UnsubscribeTopic(string country, string device, string language, string token)
        {
            var topicName = string.Empty;

            if (country == "HongKong")
            {
                topicName = AllHKUserTopic + language + device;
            }
            else
            {
                topicName = AllSGUserTopic + language + device;
            }
            var body = new
            {
                to = "/topics/" + topicName,
                registration_tokens = new string[] { token }
            };
            var JsonBody = JsonConvert.SerializeObject(body);

            var request = new HttpRequestMessage(HttpMethod.Post, UnSubscribeURL);
            request.Headers.TryAddWithoutValidation("Authorization", Authorization);
            request.Content = new StringContent(JsonBody, Encoding.UTF8, "application/json");
            try
            {
                using (var client = new HttpClient())
                {
                    var result = client.SendAsync(request).Result;
                    if (result.StatusCode == HttpStatusCode.OK)
                    {
                        log.Info("Subscribe  to " + topicName + " Sucess!");
                        log.Info(token);
                    }
                    else
                    {

                        log.Error("Subscribe to " + topicName + " Failed!");
                        log.Error(token);
                    }
                }

            }
            catch (Exception err)
            {
                log.Error("Subscribe to " + topicName + " Failed!");
                log.Error(token);
                log.Error(err);
            }
        }
    }
}

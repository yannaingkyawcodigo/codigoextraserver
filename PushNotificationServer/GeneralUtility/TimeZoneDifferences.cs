﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushNotificationServer.GeneralUtility
{
    
        public class TimeZoneDifferences
        {
            public static DateTime GetLocalTimeWithCountry(int country)
            {
                switch (country)
                {
                    case 1: return DateTime.UtcNow.AddHours(8);
                    case 2: return DateTime.UtcNow.AddHours(8);
                    case 3: return DateTime.UtcNow.AddHours(7);
                    default: return DateTime.UtcNow;
                }
            }
        }
    
}

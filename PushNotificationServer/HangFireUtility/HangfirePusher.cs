﻿using Newtonsoft.Json;
using PushNotificationServer.Model;
using System;
using System.Net;
using System.Net.Http;
using System.Text;


namespace PushNotificationServer.HangFireUtility
{
    public class HangfirePusher
    {
        static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(HangfirePusher));
        public void ScheduleSendToTopic(PushTopicModel model)
        {

            var dataBody = new
            {
                title = model.Title,
                body = model.Body,
                item_id = model.PushId,
                media_type = model.MediaType,
                media_url = model.ImageUrl,
                sound = model.Song,
                notification_type = model.NotificationType
            };
            var JsonBody = string.Empty;
            if (model.DeviceType == Enum.MobileDeviceType.iOS)
            {
                var requestBody = new
                {
                    notification = dataBody,
                    to = "/topics/" + model.TopicName,
                    priority = "high",
                    content_available = true
                };
                JsonBody = JsonConvert.SerializeObject(requestBody);
            }
            else {
                var requestBody = new
                {
                    data = dataBody,
                    to = "/topics/" + model.TopicName,
                    priority = "high",
                    content_available = true
                };
                JsonBody = JsonConvert.SerializeObject(requestBody);
            }
            
            

            var request = new HttpRequestMessage(HttpMethod.Post, model.PushURL);
            request.Headers.TryAddWithoutValidation("Authorization", model.Authorization);
            log.Info("Push body " + JsonBody);
            request.Content = new StringContent(JsonBody, Encoding.UTF8, "application/json");
            try
            {
                using (var client = new HttpClient())
                {
                    var result = client.SendAsync(request).Result;
                    if (result.StatusCode == HttpStatusCode.OK)
                    {
                        string body = result.Content.ReadAsStringAsync().Result;
                        log.Info("Push to "+model.TopicName + " Sucess!");
                        log.Info(body);
                    }
                    else
                    {
                        string body = result.Content.ReadAsStringAsync().Result;
                        log.Error("Push to " + model.TopicName + " Failed!");
                        log.Error(body);
                    }
                }

            }
            catch (Exception err) {
                log.Error("Push to " + model.TopicName + " Failed!");
                log.Error(err);
            }
        }



        

    }
}

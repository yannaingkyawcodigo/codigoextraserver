﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushNotificationServer.Model
{
    public class PushNotificationModel
    {
        public Guid Push_Notification_Id { get; set; }
        public string Push_Notification_Title { get; set; }
        public string Push_Notification_Content { get; set; }
        public string Push_Image { get; set; }
        public DateTime? Push_Schedule { get; set; }

    }
}

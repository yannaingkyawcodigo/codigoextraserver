﻿using PushNotificationServer.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushNotificationServer.Model
{
    public class PushTopicModel
    {
        public MobileDeviceType DeviceType;
        public Guid PushId { get; set; }
        public string TopicName { get; set; }
        public string Title { get; set; }

        public string Body { get; set; }
        public string Song { get; set; }

        public int MediaType { get; set; }

        public int NotificationType { get; set; }

        public string ImageUrl { get; set; }

        public string PushURL { get; set; }
        public string Authorization { get; set; }
    }
}

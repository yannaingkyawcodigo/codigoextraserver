﻿using Microsoft.Extensions.Configuration;
using PushNotificationServer.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace PushNotificationServer.DatabaseUtility
{
    public interface IDatabaseUtility
    {
        PushNotificationModel GetPushInfo(string PushId, string DatabaseName);
    }

    public class DatabaseUtility : IDatabaseUtility
    {
        private readonly string ApplicationConnectionString;
        private readonly string qGetPushInfo;


        public DatabaseUtility(IConfiguration config)
        {
            qGetPushInfo = config["Queries:PushInfo"];
            ApplicationConnectionString = config["ConnectionString:ApplicationServer"];
        }

        public PushNotificationModel GetPushInfo(string PushId, string DatabaseName)
        {
            var query = qGetPushInfo.Replace("{DatabaseName}", DatabaseName)
                .Replace("{PushId}", PushId);
            var pushDataTable = new DataTable();
            PushNotificationModel model = null;
            using (SqlConnection sqlConnection = new SqlConnection(ApplicationConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand sqlCommand = new SqlCommand(query, sqlConnection))
                {
                    new SqlDataAdapter(sqlCommand).Fill(pushDataTable);
                }
            }
            if (pushDataTable.Rows.Count > 0)
            {
                var dataRow = pushDataTable.Rows[0];
                DateTime? pushSchedule = null;
                if (dataRow["Push_Schedule"] != DBNull.Value)
                {
                    pushSchedule = (DateTime)dataRow["Push_Schedule"];
                }
                model = new PushNotificationModel
                {
                    Push_Notification_Id = Guid.Parse(PushId),
                    Push_Notification_Content = (string)dataRow["Push_Notification_Content"],
                    Push_Image = (string)dataRow["Push_Image"],
                    Push_Notification_Title = (string)dataRow["Push_Notification_Title"],
                    Push_Schedule = pushSchedule
                };
            }
            return model;
        }

    }
}

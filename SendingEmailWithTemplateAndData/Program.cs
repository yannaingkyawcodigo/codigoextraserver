﻿using System;
using System.IO;
using System.Net.Mail;
using System.Text;

namespace SendingEmailWithTemplateAndData
{
    class Program
    {
        static void Main(string[] args)
        {
            var template = File.ReadAllText("Template\\edm.html");
            var list = File.ReadAllLines("Data\\data.csv");
            foreach (var line in list)
            {
                var data = line.Split(",");
                if (data.Length > 1)
                {
                    var email = data[0];
                    var name = data[1];
                    var emailBody = template.Replace("{0}", name);
                    var subject = "Sumimasen, let's try again please!";
                    bool status = SendEmailAsync(email, subject, emailBody);
                    Console.WriteLine("Send " + (status ? "Success" : "Failed") + " for " + email);
                }
            }
        }

        public static bool SendEmailAsync(string ParmToEmail, string ParmSubject, string ParmBody)
        {
            bool Is_Sent_Success = false;
            try
            {
                
                SmtpClient Obj_Smtp_Client = new SmtpClient("smtp.office365.com");
                //Prepare Milclient
                Obj_Smtp_Client.UseDefaultCredentials = false;
                Obj_Smtp_Client.Host = "smtp.office365.com";
                Obj_Smtp_Client.DeliveryMethod = SmtpDeliveryMethod.Network;
                Obj_Smtp_Client.Port = 587;
                Obj_Smtp_Client.Credentials = new System.Net.NetworkCredential("noreply@dondondonki.com", "Pass1234");
                Obj_Smtp_Client.EnableSsl = true;
                //var fromEmail = new MailAddress("<DONDONDONKIHongKong>@dondondonki.com");
                //var toEmail = new MailAddress(ParmToEmail);
                using (MailMessage Obj_Mail_Message = new MailMessage())
                {
                    Obj_Mail_Message.From = new MailAddress("noreply@dondondonki.com", "DONDONDONKIHongKong");
                    Obj_Mail_Message.To.Add(new MailAddress(ParmToEmail));
                    Obj_Mail_Message.ReplyToList.Add(new MailAddress("support.hk@dondondonki.com", "HongKongSupport"));
                    Obj_Mail_Message.Subject = ParmSubject;
                    Obj_Mail_Message.Body = ParmBody;
                    Obj_Mail_Message.BodyEncoding = Encoding.UTF8;
                    Obj_Mail_Message.IsBodyHtml = true;
                   // Obj_Mail_Message.Headers.Add("Message-Id", String.Concat("<HK-", DateTime.Now.ToString("yyMMdd"), ".", DateTime.Now.ToString("HHmmss"), "@dondondonki.com>"));

                    Obj_Smtp_Client.Send(Obj_Mail_Message);
                }   
                
                Is_Sent_Success = true;

            }
            catch (Exception ex)
            {

            }
            return Is_Sent_Success;
        }
    }
}
